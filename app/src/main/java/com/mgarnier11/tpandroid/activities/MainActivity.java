package com.mgarnier11.tpandroid.activities;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Entreprise;
import com.mgarnier11.tpandroid.classes.EntrepriseType;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Entreprise entreprise;

    private EditText entrepriseNameInput;
    private Spinner entrepriseTypeSelect;
    private EditText entrepriseRevenueInput;
    private FloatingActionButton nextScreenButton;

    private TextWatcher onEntrepriseNameInputTextChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void afterTextChanged(Editable editable) {
            entreprise.setName(editable.toString());
        }
    };

    private TextWatcher onEntrepriseRevenueInputTextChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void afterTextChanged(Editable editable) {
            try {
                entreprise.setRevenue((int)Double.parseDouble(editable.toString()));
            } catch (Exception e) {
                entreprise.setRevenue(0);
            }
        }
    };

    private AdapterView.OnItemSelectedListener onEntrpriseTypeSelectChanged = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            entreprise.setTaxValue(EntrepriseType.values()[i].taxValue);
            entreprise.setType(EntrepriseType.values()[i].label);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    };

    private View.OnClickListener onNextScreenButtonClickListenner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,EntrepriseInformationsActivity.class);

            Bundle b = new Bundle();

            b.putParcelable("entreprise", entreprise);

            intent.putExtras(b);

            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.no_anim);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.entreprise = new Entreprise("", EntrepriseType.values()[0].taxValue, 0, EntrepriseType.values()[0].label);

        this.initializeLayoutFields();
    }

    private void initializeLayoutFields() {

        this.entrepriseNameInput =  findViewById(R.id.entrepriseNameInput);
        this.entrepriseTypeSelect =  findViewById(R.id.entrepriseTypeSelect);
        this.entrepriseRevenueInput =  findViewById(R.id.entrepriseRevenueInput);
        this.nextScreenButton =  findViewById(R.id.buttonNextScreen);

        this.entrepriseNameInput.addTextChangedListener(this.onEntrepriseNameInputTextChanged);
        this.entrepriseRevenueInput.addTextChangedListener(this.onEntrepriseRevenueInputTextChanged);

        List<EntrepriseType> typesList = Arrays.asList(EntrepriseType.values());

        String[] typesNames = new String[typesList.size()];
        for(int i = 0; i < typesList.size(); i++) {
            @StringRes int resId = getResources().getIdentifier(typesList.get(i).label, "string", getPackageName());

            typesNames[i] = getString(resId);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, typesNames);

        this.entrepriseTypeSelect.setAdapter(adapter);
        this.entrepriseTypeSelect.setOnItemSelectedListener(this.onEntrpriseTypeSelectChanged);

        this.nextScreenButton.setOnClickListener(this.onNextScreenButtonClickListenner);
    }
}