package com.mgarnier11.tpandroid.activities;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Entreprise;

public class EntrepriseInformationsActivity extends AppCompatActivity {

    private Entreprise entreprise;

    private TextView entrepriseNameValueText;
    private TextView entrepriseTypeValueText;
    private TextView entrepriseRevenueValueText;
    private TextView entrepriseContributionValueText;
    private TextView entrepriseBenefitValueText;

    private FloatingActionButton backButton;

    private View.OnClickListener onBackButtonClickListenner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
            overridePendingTransition(R.anim.no_anim, R.anim.slide_out_right);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entreprise_informations);

        Intent intent = this.getIntent();

        if (intent != null) {
            Bundle b = intent.getExtras();

            this.entreprise = (Entreprise)b.getParcelable("entreprise");
        } else {
            this.entreprise = new Entreprise("No entreprise received", 0.0, 0, "");
        }

        this.initializeFields();
    }

    private void initializeFields() {
        this.entrepriseNameValueText = findViewById(R.id.entrepriseNameValueTextView);
        this.entrepriseTypeValueText = findViewById(R.id.entrepriseTypeValueTextView);
        this.entrepriseRevenueValueText = findViewById(R.id.entrepriseRevenueValueTextView);
        this.entrepriseContributionValueText = findViewById(R.id.entrepriseContributionValueTextView);
        this.entrepriseBenefitValueText = findViewById(R.id.entrepriseBenefitValueTextView);

        this.backButton = findViewById(R.id.buttonBack);
        this.backButton.setOnClickListener(this.onBackButtonClickListenner);

        this.entrepriseNameValueText.setText(this.entreprise.getName());
        @StringRes int resId = getResources().getIdentifier(this.entreprise.getType(), "string", getPackageName());
        String type = getString(resId);

        this.entrepriseTypeValueText.setText(type + " (" + this.entreprise.getTaxValue() + "%)");
        this.entrepriseRevenueValueText.setText(this.entreprise.getRevenue() + " €");

        long contribution = Math.round(this.entreprise.getRevenue() * this.entreprise.getTaxValue() / 100);
        long benefit = this.entreprise.getRevenue() - contribution;

        this.entrepriseContributionValueText.setText(contribution + " €");
        this.entrepriseBenefitValueText.setText(benefit + " €");

    }
}