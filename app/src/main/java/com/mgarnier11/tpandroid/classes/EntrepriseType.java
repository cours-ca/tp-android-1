package com.mgarnier11.tpandroid.classes;

public enum EntrepriseType {
    MerchandiseSale("merchandise_sale", 12.8),
    FurnishedAccomodationRental("furnished_accomodation_rental", 22.0),
    TourismFurnishedAccomodationRental("tourism_furnished_accomodation_rental", 6.0),
    ServiceProvision("service_provision", 22.0),
    LiberalProfession("liberal_profession", 22.0);



    public final double taxValue;
    public final String label;

    EntrepriseType(String l, double v) {
        this.label = l;
        this.taxValue = v;
    }
}
