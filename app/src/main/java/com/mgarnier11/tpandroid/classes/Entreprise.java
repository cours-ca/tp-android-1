package com.mgarnier11.tpandroid.classes;

import android.os.Parcel;
import android.os.Parcelable;

public class Entreprise implements Parcelable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(double taxValue) {
        this.taxValue = taxValue;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String name;
    private String type;
    private double taxValue;
    private long revenue;


    public Entreprise(String _name, double _taxValue, long _revenue, String _type) {
        this.name = _name;
        this.taxValue = _taxValue;
        this.revenue = _revenue;
        this.type = _type;
    }

    Entreprise(Parcel input) {
        this.name = input.readString();
        this.taxValue = input.readDouble();
        this.revenue = input.readLong();
        this.type = input.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeDouble(this.taxValue);
        dest.writeLong(this.revenue);
        dest.writeString(this.type);
    }

    public static final Parcelable.Creator<Entreprise> CREATOR
            = new Parcelable.Creator<Entreprise>() {
        @Override
        public Entreprise createFromParcel(Parcel source) {
            return new Entreprise(source);
        }
        @Override
        public Entreprise[] newArray(int size) {
            return new Entreprise[size];
        }
    };
}
